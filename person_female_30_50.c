#include <stdio.h>

/* UDC Spring 2014 CSCI315
   Assignment #6
   Unix Systems Programming */

int main()
{
	FILE *rFile, *wFile;
	rFile = fopen("wash_dc.txt", "r");
	wFile = fopen("output.txt", "w");
	long int fileData[4];

	int fems = 0; // keeps track of females for each household ID matching selection criteria
	int numFems = 0; // tracks number of total females matching selection criteria
	long int houseID = 0; // keeps track of last household ID from previous line read
	int i = 0; // counter that keeps track of how many incidents of 2 or more females that live in one household that match selection criteria
	int lines = 0; // line counter
	int numHouseIDs = 0; // tracks total number of households with more than 2 occupants

	if (rFile==NULL) perror ("Error opening file");

	while (!feof(rFile)) {
		lines++;
		fscanf(rFile, "%ld %ld %ld %ld", fileData, fileData+1, fileData+2, fileData+3);
	
		if ((fileData[3] == 2) && (fileData[2] > 30) && (fileData[2] < 50)) {
			numFems += 1;
			fems += 1;
			fprintf(wFile, "%ld\n", fileData[1]);
		}
		else {
			if (i == 0) {
				fems = 0;
			}
		}
		if ((houseID == fileData[0]) && (fems > 2) && (fileData[3] == 2) && (fileData[2] > 30) && (fileData[2] < 50)) {
			i += 1;
			if (i > 1) {
				numHouseIDs += 1;
				i = fems = 0;
			}
		}
		else {
			if (houseID != fileData[0]) {
			i = 0;
			}
		}

		houseID = fileData[0];
	}

	printf("Numer of lines read: %i\n", lines);
	printf("Total number of females older than 30 and younger than 50: %i\n", numFems);
	printf("Total number of households having more than 2 females that live together and that are older than 30 and younger than 50: %i\n", numHouseIDs);


	// close file
	fclose(rFile);
	fclose(wFile);

	return 0;
}
