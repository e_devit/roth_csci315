#!/bin/bash

# UDC Spring 2014 CSCI315
# Assignment #5
# Unix Systems Programming

exec 3< $1 # file to open and process
exec 4> $2 # file to output results to

fems=0 # keeps track of females for each household ID matching selection criteria
numFems=0 # tracks number of total females matching selection criteria
houseID=0 # keeps track of last household ID from previous line read
i=0 # counter that keeps track of how many incidents of 2 or more females that live in one household that match selection criteria
numHouseIDs=0 # tracks total number of households with more than 2 occupants

while read -u 3 a b c d
do
	if [ "$d" == "2" ] && [ "$c" -gt "30" ] && [ "$c" -lt "50" ]
	then
		numFems=$(( numFems+1 ))
		fems=$(( fems+1 ))
		echo $b>&4
		echo "matching Household ID with more than 2:" $a "female ID:" $b "fems:" $fems
	else
		if [ "$i" == "0" ]
		then
			fems=0
		fi
	fi	
	if [ "$houseID" -eq "$a" ] && [ "$fems" -gt "2" ] && [ "$d" == "2" ] && [ "$c" -gt "30" ] && [ "$c" -lt "50" ]
	then
		i=$(( i+1 ))
		echo "Household1 ID with more than 2: " $a
		echo "fem ID with more than 2: " $b
		echo "i: " $i
		if [ $i -ge 1 ]
		then
			echo "Household ID with more than 2: " $a
			numHouseIDs=$(( numHouseIDs+1 ))
			i=0
			fems=0
		fi
	else
		if [ "$houseID" -ne "$a" ]
		then
			i=0
		fi
	fi
	houseID=$a
done

echo "Total number of females older than 30 and younger than 50: " $numFems
echo "Total number of households having more than 2 females that live together and that are older than 30 and younger than 50: "$numHouseIDs

# close file
exec 4>&-
